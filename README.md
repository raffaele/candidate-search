# Candidate search engine

## Requirements

You are provided with a sample list of candidates in a .json file and a very basic design, that gives you an idea of how we want our internal API tool to look like.

The task is to use the sample candidates list, provided in mock-candidates.json and build the UI ahead of time - for internal user testing - so that when the back-end developers are done with the development of the API, we can just switch to calling the real endpoints and get real data.

If you want to use some small utility library like lodash for some javascript functionality, this is ok but please don’t use any other libraries or frameworks apart from that.

## INSTRUCTIONS

1. The user should be able to click on the “Get candidates” button and see a list of candidates coming back from the server (a mock list of 10 candidates in our case - since we don’t have any server-side code).

2. When the list of candidates comes back, the user can see how many candidates are displayed.

3. When the user clicks on a candidate’s name on the list, they see a small preview of the candidate’s profile on the right hand-side.

4. On the candidate’s profile preview, the user can see the details of the candidate that appear on the list as well, together with some skills this candidate has.

5. Please develop if you have time. On the candidate’s profile view, the user can see a “Shortlist” button as per the design. When they click on the shortlist button, the button should go red and it’s text should change to “Shortlisted!”. Also, the counter above the candidate profile that counts how many candidates are shortlisted, should increment.

# WHAT I DID

I used webpack and babel to transpile the ES6 code in ES5, so the result js file will be ES5 javascript compatible with IE9. I had not the possibility to check it on IE browser (I have a mac laptop). I tried to implement the code to be compatible with the requirement, but I was based just on my memory.

The `server` folder contains the mock data, a mock server is execute with [http-server](https://www.npmjs.com/package/http-server). All the source code is in `src` folder, interaction with API are managed in `src/ajax` folder (in `api.js` file there is the code and in `config.json` the configuration for the end point). All the interaction with the dom is managed in `src/domManager.js` and in `src/lib.js` I implemented a simple forEach method (I'm not sure I can use [Array.forEach](https://caniuse.com/#search=forEach) in IE9).

# STEP TO EXECUTE THE APP

1. Install dependencies (run `npm install` from command line)
2. Execute the command `npm start` (it will start the server and compile the ES6 code in ES5).
3. Open in the browser the file `./dist/index.html`.
