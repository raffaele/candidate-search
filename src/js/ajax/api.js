import * as conf from './config.json';

const api = {
    getMatches: getMatches
};

function getMatches () {
    const http = new XMLHttpRequest(),
        url = conf.path + conf.endPoint;

    http.open("GET", url, true);

    http.send();
    
    return new Promise ((resolveFn, rejectFn)=>{
        http.onreadystatechange = () => {
            if (http.readyState !== 4) return;
            let jsonResponse = getJsonResponse(http.response);
            http.status === 200 ? resolveFn(jsonResponse) : rejectFn(jsonResponse)
        };
    });
}

function getJsonResponse (httpResponse) {
    return typeof(JSON) === 'undefined' ?
        eval (httpResponse) :
        JSON.parse(httpResponse);
}

export {api};