export {createCandidateSimpleView, fillCandiateDetailsDom};

import {getById, createDom} from './domManager';
import {each} from './lib';

function createCandidateSimpleView (candidate) {
    let mainDom = createDom('li', ['candidate']);
    let nameDom = createNameDom(candidate.Name);
    let jobDetailsDom = createJobDetailsDom(candidate.Salary,
        candidate.MostRecentEmployment,
        candidate.MostRecentEmploymentDurationInYears,
        candidate.CurrentLocation,
        candidate.DistanceInMiles);

    mainDom.appendChild(nameDom);
    mainDom.appendChild(jobDetailsDom);
    
    return mainDom;
}

function createJobDetailsDom (salary, recentEmployment, emplDuration, location, distance) {
    let mainDom = createDom('div', ['job-details']);
    let salaryDom = createDivByContent('Min ' + salary);
    let emplDom = createDivByContent(recentEmployment + ' (' + emplDuration + ' yrs)');
    let distanceDom = createDivByContent(location + ' (' + distance + ' miles away)');

    mainDom.appendChild(salaryDom);
    mainDom.appendChild(emplDom);
    mainDom.appendChild(distanceDom);

    return mainDom;
}

function createDivByContent (content) {
    let dom = createDom('div');
    dom.innerHTML = content;
    return dom;
}

function createNameDom (name) {
    let dom = createDom('div', ['name']);
    dom.innerHTML = name;

    return dom;
}

function fillCandiateDetailsDom (candidate) {
    showSelectedCandidateName(candidate);
    showSelectedCandidateSkills(candidate);
    showSelectedCandidateSynthesis(candidate);
}

function showSelectedCandidateSynthesis (candidate) {
    getById('detail-old-emply').innerHTML = candidate.MostRecentEmployment;
    getById('detail-salary-required').innerHTML = candidate.Salary;
    getById('detail-position').innerHTML = candidate.CurrentLocation;
}

function showSelectedCandidateName (candidate) {
    getById('details-name').innerHTML = candidate.Name;
}

function showSelectedCandidateSkills (candidate) {
    let detailsSkillsDom = getById('details-skills');
    detailsSkillsDom.innerHTML = '';
    each(candidate.Skills, (skill) => {
        let skillDom = createDom('li');
        skillDom.innerHTML = skill;
        detailsSkillsDom.appendChild(skillDom);
    });
    
}
