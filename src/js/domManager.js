export {getById, createDom};
import {each} from './lib';

function getById (id) {
    return document.getElementById(id);
}

function createDom (tagName, classes, children) {
    let dom = document.createElement(tagName);
    if (classes) {
        each(classes, className => dom.classList.add(className));
    }
    return dom;
}
