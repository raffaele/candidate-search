export { each };

function each (array, callback) {
    if (!array) return;
    for (let ii = 0, maxLength = array.length; ii < maxLength; ii++) {
        callback(array[ii], ii, array);
    }
}