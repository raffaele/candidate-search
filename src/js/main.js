import {api} from './ajax/api';
import {getById, createDom} from './domManager';
import {each} from './lib';

import {createCandidateSimpleView, fillCandiateDetailsDom} from './candidateDom';

const css = require('./main.css');

const candidateListDom = getById('candidate-list'),
    addToShortListBtn = getById('add-to-shortlist-cmd'),
    removeFromShortListBtn = getById('remove-from-shortlist-cmd'),
    shortlistedCandidatesCounterDom = getById('shortlisted-candidates');

let selectedMatch = null,
    matches = [],
    shortListedCounter = 0;

addToShortListBtn.addEventListener('click', () => {
    if (!selectedMatch) return;
    selectedMatch.inShortlist = true;
    updateShortlistedBtns();
    updateShortlistedCounter(1);
});
removeFromShortListBtn.addEventListener('click', () => {
    if (!selectedMatch) return;
    selectedMatch.inShortlist = false;
    updateShortlistedBtns();
    updateShortlistedCounter(-1);
});

getById('get-cadidates-cmd').addEventListener('click', ()=>{
    api.getMatches().then(showResults);
});

function showResults (response) {
    matches = response.Matches;
    getById('result-synthesis').classList.remove('hidden-dom');
    getById('total-candidates').innerHTML = response.NumberOfMatches;
    shortListedCounter = 0;
    candidateListDom.innerHTML = '';

    each(matches, (match) => {
        if (match.inShortlist) {
            shortListedCounter++;
        }
        let matchDom = createCandidateSimpleView(match);
        matchDom.addEventListener('click', () => {
            deselectMatchDom();
            selectedMatch = match;
            matchDom.classList.add('selected');
            fillCandiateDetailsDom(match);
            getById('candidate-details').classList.remove('hidden-dom');
            updateShortlistedBtns();
        });
        candidateListDom.appendChild(matchDom);
    });
    updateShortlistedCounter();
}

function updateShortlistedBtns () {
    if (!selectedMatch) return;
    if (selectedMatch.inShortlist) {
        addToShortListBtn.classList.add('hidden-dom');
        removeFromShortListBtn.classList.remove('hidden-dom');
    } else {
        addToShortListBtn.classList.remove('hidden-dom');
        removeFromShortListBtn.classList.add('hidden-dom');
    }
}

function updateShortlistedCounter (increment) {
    if (increment) {
        shortListedCounter += increment;
    }

    shortlistedCandidatesCounterDom.innerHTML = shortListedCounter;
}



function deselectMatchDom () {
    var selectedMatchDom = candidateListDom.getElementsByClassName('selected');
    if (selectedMatchDom.length === 0) return;
    selectedMatchDom[0].classList.remove('selected');
}
